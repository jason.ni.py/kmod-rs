#![no_std]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]

use cty::c_void;

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

pub const GFP_KERNEL: gfp_t = EX_GFP_KERNEL;

pub unsafe fn kmalloc(size: usize, flags: gfp_t) -> *mut c_void {
    __kmalloc(size, flags)
}
